module.exports = [
  {
    test: /\.js?$/,
    exclude: /(node_modules|bower_components|public)/,
    loader: "babel-loader"
  },
  {
    test: /\.(eot|svg|ttf|woff|woff2)$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'file?name=dev/fonts/[name].[ext]'
  },
  {
    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
    exclude: /(node_modules|bower_components)/,
    loader: "url?limit=10000&mimetype=image/svg+xml"
  },
  {
    test: /\.jpg/,
    exclude: /(node_modules|bower_components)/,
    loader: "url-loader?limit=10000&mimetype=image/jpg"
  },
  {
    test: /\.png/,
    exclude: /(node_modules|bower_components)/,
    loader: "url-loader?limit=10000&mimetype=image/png"
  },
  {
    test: /\.mp4/,
    exclude: /(node_modules|bower_components)/,
    loader: "url-loader?limit=10000&mimetype=video/mp4"
  },
  {
    test: /\.scss$/,
    loaders: ["style-loader", "css-loader", "postcss-loader", "sass-loader"]
  }
];
