'use strict';

const webpack = require("webpack");
const loaders = require('./webpack.loaders');
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  context: __dirname + "/src",
  entry: {
    app: "./index.js"
  },
  module: {
    loaders
  },
  output: {
    path: __dirname + "/dev",
    filename: "kuri.bundle.js",
    libraryTarget: "var",
    library: "kuri"
  },
  plugins: [
    //new CopyWebpackPlugin([
    //  { from: '../assets' }
    //]),
  ],
  stats: {
      colors: true,
      hash: false,
      version: false,
      timings: false,
      assets: true,
      chunks: false,
      modules: false,
      reasons: false,
      children: false,
      source: false,
      errors: false,
      errorDetails: false,
      warnings: false,
      publicPath: false
  }
};
