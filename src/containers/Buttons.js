import { connect } from 'react-redux'
import { next, prev, play, reverse, jumpToSlide } from '../actions'
import Links from '../components/Links'

const handlePrev = (dispatch, isPlaying) => {
  if (isPlaying) {
    dispatch(reverse());
  } else {
    dispatch(prev());
  }
};

const handleNext = (dispatch, isPlaying) => {
  if (!isPlaying) {
    dispatch(play());
  } else {
    dispatch(next());
  }
};

const mapStateToProps = (state) => {
  return {
    activeSlide: state.kuri[0].activeSlide,
    deckLength: state.kuri[0].deckLength,
    isPlaying: state.kuri[0].isPlaying,
    inReverse: state.kuri[0].inReverse
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    jumpToSlide: (id) => {
      return dispatch(jumpToSlide(id));
    },
    handleArrow: _.debounce((type, isPlaying) => {
      switch (type) {
        case 'next':
          return handleNext(dispatch, isPlaying);
        case 'prev':
          return handlePrev(dispatch, isPlaying)
      }
    }, 150, {
      'leading': true,
      'trailing': false
    })
  }
};

const Buttons = connect(
  mapStateToProps,
  mapDispatchToProps
)(Links);

export default Buttons
