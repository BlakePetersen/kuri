import { render } from 'react-dom'
import { connect } from 'react-redux'
import { next, prev, play, stop, reverse, copy } from '../actions'
import Slide from '../components/Slide'
import SlideData from '../defaults';
const makeVideoPlayableInline = require('iphone-inline-video');

const _getSlideContent = (slide) => {
  return SlideData[slide];
};

const _makeVideoInline = (video) => {
  makeVideoPlayableInline(video);
};

const handleNext = (dispatch) => {
  dispatch(play());
};

const mapStateToProps = (state, {assetPaths}) => {
  const nextSlide = (state.kuri[0].activeSlide < state.kuri[0].deckLength - 1) ? state.kuri[0].activeSlide + 1 : state.kuri[0].activeSlide;
  return {
    slide: _getSlideContent(state.kuri[0].activeSlide),
    nextSlideContent: _getSlideContent(nextSlide),
    activeSlide: state.kuri[0].activeSlide,
    isPlaying: state.kuri[0].isPlaying,
    inReverse: state.kuri[0].inReverse,
    canReverse: state.kuri[0].canReverse,
    showCopy: state.kuri[0].showCopy,
    deckLength: state.kuri[0].deckLength,
    assetPaths: assetPaths
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleOnEnded: (direction) => {
      dispatch(stop());
      switch (direction) {
        case 'forward':
          return dispatch(next());
        case 'reverse':
          return false;
        default:
          return false;
      }
    },
    handleNext: () => {
      handleNext(dispatch)
    },
    handleOnLoaded: (video) => {
      _makeVideoInline(video);
    },
    handleShowCopy: () => {
      dispatch(copy())
    }
  }
};

const VisibleSlide = connect(
  mapStateToProps,
  mapDispatchToProps
)(Slide);

export default VisibleSlide
