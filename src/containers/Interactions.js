import reducer from '../reducers'
import { render } from 'react-dom'
import { connectWithLifecycle } from 'react-lifecycle-component'
import { next, prev, play, reverse, loaded } from '../actions'
import Interaction from '../components/Interaction'
import _ from 'lodash'



const handlePrev = (dispatch, isPlaying, canReverse) => {
  if (!isPlaying && canReverse) {
    dispatch(prev());
  }
};

const handleNext = (dispatch, isPlaying, inReverse) => {
  if (!isPlaying && !inReverse) {
    dispatch(play());
  }
};

const _returnFocus = () => {
  const target = document.querySelector(".interaction"),
        links = document.querySelector(".slide-links");

  if(!target)
    return false;

  target.focus();
  links.blur();
};

const _resizeTarget = () => {
  const target = document.querySelector("#kuri_video"),
    img = document.querySelector(".imgCache");
  const aspectRatio = img.clientHeight / img.clientWidth;
  target.width = window.innerWidth;
  target.height = window.innerWidth * aspectRatio;
};

const _windowScrollEventHandler = (dispatch) => {
  window.addEventListener('mousewheel', _.debounce((e) => {
    const target = document.querySelector('.interaction');

    const isPlaying = JSON.parse(target.dataset.isplaying),
          inReverse = JSON.parse(target.dataset.inreverse),
          canReverse = JSON.parse(target.dataset.canreverse);

    e.preventDefault();
    if (e.deltaY < 0) {
      handlePrev(dispatch, isPlaying, canReverse)
    } else if (e.deltaY > 0) {
      handleNext(dispatch, isPlaying, inReverse)
    }
  }, 500, {
    'leading': true,
    'trailing': false,
    'maxWait': 2000
  }))
};

const _windowResizeEventHandler = () => {
  window.addEventListener('resize', _.debounce(() => {
    _resizeTarget()
  }, 50, {
    'leading': false,
    'trailing': true
  }))
};

const _componentDidUpdate = () => {
   _returnFocus();
};

const mapStateToProps = (state) => {
  return {
    activeSlide: state.kuri[0].activeSlide,
    isPlaying: state.kuri[0].isPlaying,
    inReverse: state.kuri[0].inReverse,
    canReverse: state.kuri[0].canReverse,
    showCopy: state.kuri[0].showCopy,
    deckLength: state.kuri[0].deckLength,
    componentDidUpdate: _componentDidUpdate(state.kuri[0]),
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleOnKeyPress: _.debounce((e, isPlaying, canReverse, inReverse) => {
      switch (e.key) {
        case 'ArrowDown':
        case 'ArrowRight':
        case ' ':
          e.preventDefault();
          return handleNext(dispatch, isPlaying, inReverse);
        case 'ArrowUp':
        case 'ArrowLeft':
        case 'Backspace':
          e.preventDefault();
          return handlePrev(dispatch, isPlaying, canReverse)
      }
    }, 150, {
      'leading': true,
      'trailing': false
    }),
    handleNext: (isPlaying, inReverse) => {
      handleNext(dispatch, isPlaying, inReverse)
    },
    handlePrev: (isPlaying, canReverse) => {
      handlePrev(dispatch, isPlaying, canReverse)
    },
    componentDidMount: () => {
      _returnFocus();
      _windowScrollEventHandler(dispatch);
      _windowResizeEventHandler(dispatch);
    },
  }
};

const Interactions = connectWithLifecycle(
  mapStateToProps,
  mapDispatchToProps
)(Interaction);

export default Interactions
