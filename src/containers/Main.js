import React from 'react'
import { connect } from 'react-redux'
import { next, prev, play, stop, reverse } from '../actions'
import App from '../components/App'
import _ from 'lodash'

const handlePrev = (dispatch, isPlaying, canReverse) => {
  if (isPlaying && canReverse) {
    dispatch(reverse());
  } else {
    dispatch(prev());
  }
};

const handleNext = (dispatch, isPlaying) => {
  if (!isPlaying) {
    dispatch(play());
  } else {
    dispatch(next());
  }
};

const mapStateToProps = (state, {assetPaths}) => {
  return {
    activeSlide: state.kuri[0].activeSlide,
    isPlaying: state.kuri[0].isPlaying,
    inReverse: state.kuri[0].inReverse,
    canReverse: state.kuri[0].canReverse,
    assetPaths: assetPaths
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleNext: _.debounce((isPlaying) => {
      return handleNext(dispatch, isPlaying);
    }, 1000, {
      'leading': true,
      'trailing': false
    }),
    handlePrev: _.debounce(() => {
      return handlePrev(dispatch);
    }, 1000, {
      'leading': true,
      'trailing': false
    })
  }
};

const Main = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default Main
