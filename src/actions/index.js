export const next = (state) => {
  return {
    type: 'NEXT',
    state
  }
}

export const prev = (state) => {
  return {
    type: 'PREV',
    state
  }
}

export const play = (state) => {
  return {
    type: 'PLAY',
    state
  }
}

export const reverse = (state) => {
  return {
    type: 'REVERSE',
    state
  }
}

export const stop = (state) => {
  return {
    type: 'STOP',
    state
  }
}

export const copy = (state) => {
  return {
    type: 'SHOW_COPY',
    state
  }
}

export const jumpToSlide = (id) => {
  return {
    type: 'JUMP_TO_SLIDE',
    id: id
  }
}

export const loaded = () => {
  return {
    type: 'LOADED'
  }
};
