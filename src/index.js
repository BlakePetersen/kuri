import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducer from './reducers'
import Main from './containers/Main'
let store = createStore(reducer);

// store.subscribe(() => {
//   console.log("store change", store.getState());
// });

const init = (el, assetPaths, aspectRatio) => {


  let paths = assetPaths || {
      images: 'images',
      videos: 'videos'
    },
    state = store.getState().kuri[0],
    width = state.width,
    height = state.height,
    ratio = aspectRatio || width / height;

  render(
    <Provider
      store={store}>
      <Main
        assetPaths={paths}
        aspectRatio={ratio}
      />
    </Provider>,
    document.getElementById(el)
  );
};

const destroy = (el) => {
  unmountComponentAtNode(document.getElementById(el))
};

module.exports = {
  init: init,
  destroy: destroy
};
