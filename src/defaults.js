const SlideData = [
  {
    header: "Explore Kuri",
    copy: "Every component in Kuri works<br>" +
    "to create dynamic interactions<br>" +
    "that brings out a real sense<br>" +
    "of personality.<br><br>" +
    "Scroll to take a deep dive into<br>" +
    "Kuri’s inner workings.<br><br>",
    image: "kuri-00.jpg",
    nextImage: "kuri-01.jpg",
    videoSrc: "kuri-00.mp4"
  },
  {
    header: "Cap Touch<br>Sensor",
    copy: "Capacitive touch sensors let<br>" +
    "Kuri respond to human touch<br>" +
    "in a truly personal way. A<br>" +
    "gentle tap to the head will<br>" +
    "make Kuri look up at you<br>" +
    "and chirp affirmingly.",
    image: "kuri-01.jpg",
    nextImage: "kuri-02.jpg",
    videoSrc: "kuri-01.mp4"
  },
  {
    header: "Camera",
    copy: "Kuri's camera is neatly located<br>" +
    "behind one of his eyes. It can<br>" +
    "capture HD pictures and videos<br>" +
    "in a wide range of lighting<br>" +
    "conditions. Livestream capabilities<br>" +
    "let Kuri be your eyes around the<br>" +
    "house when you’re not there.",
    image: "kuri-02.jpg",
    nextImage: "kuri-03.jpg",
    videoSrc: "kuri-02.mp4"
  },
  {
    header: "Gestural<br>Mechanics",
    copy: "Thanks to intricate <br>" +
    "gestural mechanics, Kuri's<br>" +
    "head and eye movements<br>" +
    "look fluid and natural. Her<br>" +
    "eyes can blink, smile, and<br>" +
    "she can look up, down,<br>" +
    "and from side to side.",
    image: "kuri-03.jpg",
    nextImage: "kuri-04.jpg",
    videoSrc: "kuri-03.mp4"
  },
  {
    header: "Microphones",
    copy: "Kuri is equipped with four<br>" +
    "sensitive microphones that help<br>" +
    "him precisely locate sounds.<br>" +
    "That means Kuri can react to<br>" +
    "your voice, or detect unusual<br>" +
    "sounds and know which way<br>" +
    "to go to investigate.",
    image: "kuri-04.jpg",
    nextImage: "kuri-05.jpg",
    videoSrc: "kuri-04.mp4"
  },
  {
    header: "Heart Light",
    copy: "A soft, warm light helps Kuri<br>" +
    "convey a sense of mood. With<br>" +
    "colors representing different<br>" +
    "emotional states, you’ll always<br>" +
    "know when Kuri is happy, or if<br>" +
    "he’s just thinking.",
    image: "kuri-05.jpg",
    nextImage: "kuri-06.jpg",
    videoSrc: "kuri-05.mp4"
  },
  {
    header: "Speakers",
    copy: "Kuri’s two speakers deliver<br>" +
    "a rich sound experience,<br>" +
    "powerful enough to fill a<br>" +
    "room. So whether Kuri is<br>" +
    "playing music, podcasts,<br>" +
    "repeating what you say,<br>" +
    "or speaking robot, you’ll<br>" +
    "always hear every detail.",
    image: "kuri-06.jpg",
    nextImage: "kuri-07.jpg",
    videoSrc: "kuri-06.mp4",
    showCopyTiming: 4
  },
  {
    header: "Mapping<br>Sensors",
    copy: "Mapping Sensors enable<br>" +
    "Kuri to navigate your <br>" +
    "house autonomously. A<br>" +
    "sensor array helps Kuri<br>" +
    "learn and remember where <br>" +
    "everything is located, and <br>" +
    "also stay away from cliffs<br>" +
    "and obstacles in his path.",
    image: "kuri-07.jpg",
    nextImage: "kuri-08.jpg",
    videoSrc: "kuri-07.mp4"
  },
  {
    header: "Drive System",
    copy: "A set of small, yet powerful,<br>" +
    "electric motors help Kuri move<br>" +
    "effortlessly around your home.<br>" +
    "His wheels are big enough to<br>" +
    "handle a wide range of flooring<br>" +
    "and carpets—even thresholds.",
    image: "kuri-08.jpg",
    nextImage: "kuri-09.jpg",
    videoSrc: "kuri-08.mp4"
  },
  {
    header: "Processor",
    copy: "You'll quickly learn how smart<br>" +
    "Kuri is. His powerful processors<br>" +
    "handle everything from facial and<br>" +
    "speech recognition, to mapping<br>" +
    "and entertaining.",
    image: "kuri-09.jpg",
    nextImage: "kuri-10.jpg",
    videoSrc: "kuri-09.mp4"
  },
  {
    header: "Charging Pad",
    copy: "The charging pad becomes Kuri’s<br>" +
    "own little spot in your house. It's<br>" +
    "where Kuri automatically goes for a<br>" +
    "power nap between activities and<br>" +
    "tasks, or when energy is running low.",
    image: "kuri-10.jpg",
    nextImage: "kuri-11.jpg",
    videoSrc: "kuri-10.mp4"
  },
  {
    header: "",
    copy: "",
    //"<a href='#'>View Gallery" +
    //"<svg height='19' width='24' viewBox='0 0 64 64'>" +
    //"<polygon fill='none' stroke='#000000' stroke-width='6' points='5,5 5,55 45,32.5'/><polygon>" +
    //"</svg>" +
    //"</a> <a href='#'>Tech Specs " +
    //"<svg height='19' width='24' viewBox='0 0 64 64'>" +
    //"<polygon fill='none' stroke='#000000' stroke-width='6' points='5,5 5,55 45,32.5'/><polygon>" +
    //"</svg>" +
   // "</a>",
    image: "kuri-11.jpg",
    nextImage: "kuri-11.jpg",
    videoSrc: "",
    isEndcap: true
  }
];

export default SlideData