import SlideData from '../defaults';

const kuri = (state = [{
  isLoaded: false,
  activeSlide: 0,
  isPlaying: false,
  inReverse: false,
  deckLength: SlideData.length,
  canReverse: true,
  showCopy: true,
  width: 1280,
  height: 720
}], action) => {

  let currentSlide = state[0].activeSlide,
      isPlaying = state[0].isPlaying,
      canReverse = state[0].canReverse;

  switch (action.type) {

    case 'NEXT':
      const notLastSlide = (currentSlide < state[0].deckLength - 1);
      notLastSlide ? currentSlide++ : currentSlide;
      return state.map((kuri) => {
        return Object.assign({}, kuri, {
          activeSlide: currentSlide,
        })
      });

    case 'PLAY':
      const shouldPlay = (state[0].deckLength - 1 != currentSlide);
      return state.map((kuri) => {
        return Object.assign({}, kuri, {
          isPlaying: shouldPlay,
          inReverse: false,
          showCopy: (currentSlide == state[0].deckLength - 1)
        })
      });

    case 'REVERSE':
      return state.map((kuri) => {
        return Object.assign({}, kuri, {
          inReverse: true,
          showCopy: false
        })
      });

    case 'STOP':
      return state.map((kuri) => {
        return Object.assign({}, kuri, {
          isPlaying: false,
          inReverse: false,
          showCopy: true
        })
      });

    case 'PREV':
      const notFirstSlide = (currentSlide > 0);
      notFirstSlide && !isPlaying ? currentSlide-- : currentSlide;
      return state.map((kuri) => {
        return Object.assign({}, kuri, {
          activeSlide: currentSlide,
          isPlaying: false,
          inReverse: notFirstSlide && canReverse,
          showCopy: !notFirstSlide
        })
      });

    case 'SHOW_COPY':
      return state.map((kuri) => {
        return Object.assign({}, kuri, {
          showCopy: true
        })
      });

    case 'JUMP_TO_SLIDE':
      return state.map((kuri) => {
        return Object.assign({}, kuri, {
          activeSlide: action.id,
          isPlaying: false,
          showCopy: true,
        })
      });

    case 'LOADED':
      return state.map((kuri) => {
        return Object.assign({}, kuri, {
          isLoaded: true
        })
      });

    default:
      return state;
  }
};

export default kuri;
