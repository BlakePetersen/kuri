import { combineReducers } from 'redux'
import kuri from './kuri'

const reducer = combineReducers({
  kuri
});

export default reducer
