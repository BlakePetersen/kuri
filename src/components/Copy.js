import React from 'react'

const Copy = ({ handleNext, isPlaying, inReverse, showCopy, isEndcap, activeSlide, slide, nextSlideContent }) => {

  function createMarkup(string) {
    return {__html: string};
  }

  let header = slide.header;
  let copy = slide.copy;
  let active = activeSlide;

  if (isPlaying && showCopy || inReverse) {
    header = nextSlideContent.header;
    copy = nextSlideContent.copy;
    active = active + 1;
  }

  return (
    <div
      className={ 'copy'
      + (isEndcap ? ' end-cap' : '')
      + (showCopy ? ' show-copy' : '')
      }
    >
      <h3 dangerouslySetInnerHTML={createMarkup(header)}/>
      <p dangerouslySetInnerHTML={createMarkup(copy)}/>
      <button
        onClick={e => {
          e.preventDefault();
          handleNext();
        }}
        style={{display: active == 0 ? 'block' : 'none'}}
      >
        <svg height='36' width='36' viewBox='0 0 64 64' style={{display: 'block', margin: '0 auto'}}>
          <polyline fill='none' stroke='#000000' strokeWidth='5' points='5,5 32.5,32.5 60,5' />
        </svg>
      </button>
    </div>
  )
}

export default Copy
