import React from 'react'

const Interaction = ({ handleOnKeyPress, isPlaying, canReverse, inReverse }) => {
  return (
    <div
      className="interaction"
      tabIndex="0"
      data-isPlaying={isPlaying}
      data-inReverse={inReverse}
      data-canReverse={canReverse}
      onKeyDown={e => {
        handleOnKeyPress(e, isPlaying, inReverse)
      }}
    ></div>
  );
};

export default Interaction
