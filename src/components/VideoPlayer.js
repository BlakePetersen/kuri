import React from 'react'

const VideoPlayer = ({ handleOnEnded, handleOnLoaded, handleShowCopy, videoSrc, isPlaying, inReverse,
  showCopyTiming, aspectRatio }) => {

  let videoElem = document.getElementById('kuri_video'),
    width = window.innerWidth,
    height = Math.floor(width * (1 / aspectRatio));

  const rewind = () => {
    window.rewindInterval = setInterval(() => {
      if (videoElem.currentTime == 0) {
        clearInterval(window.rewindInterval);
        handleOnEnded('reverse');
      } else {
        videoElem.currentTime -= .05;
      }
    }, 40);
  };

  const _playReverse = (videoElem, isPlaying) => {
    if (isPlaying) {
      videoElem.pause();
      rewind()
    } else {
      videoElem.currentTime = videoElem.duration - .001;
      videoElem.pause();
      setTimeout(() => {
        videoElem.oncanplaythrough = rewind();
      }, 150);
    }
  };

  const _copyTiming = (showCopyTiming) => {

    showCopyTiming = showCopyTiming || (videoElem.duration - .75);

    window.showCopyTimingInterval = setInterval(() => {
      if(videoElem.currentTime > showCopyTiming) {
        clearInterval(window.showCopyTimingInterval);
        handleShowCopy();
      }
    }, 40);

  };

  // Pause The video when not in use
  if (videoElem && !isPlaying && !inReverse) {
    clearInterval(window.showCopyTimingInterval);
  }

  // Play Video
  if (isPlaying && !inReverse) {
    clearInterval(window.showCopyTimingInterval);
    _copyTiming(showCopyTiming);
    clearInterval(window.rewindInterval);
    videoElem.play();
  }

  if (isPlaying && inReverse) {
    _playReverse(videoElem, isPlaying);
  }

  return (
    <video
      id="kuri_video"
      className={ 'video'
        + (isPlaying || inReverse ? ' is-playing' : '')
      }
      preload="auto"
      src={videoSrc}
      poster="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
      width={width}
      height={height}
      playsInline
      loop={false}
      type="video/mp4"
      autoPlay={false}
      onEnded={() => {
        handleOnEnded(inReverse ? 'reverse' : 'forward')
      }}
      onLoadedData={(e) => {
         handleOnLoaded(e.target, isPlaying, inReverse);
        if (inReverse) {
          _playReverse(e.target, isPlaying);
        }
      }}
    />
  );
};

export default VideoPlayer
