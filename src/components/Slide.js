import React from 'react'
import Copy from './Copy'
import VideoPlayer from './VideoPlayer'

const Slide = ({ handleNext, handleOnEnded, handleOnLoaded, handleShowCopy, slide,
  nextSlideContent, activeSlide, isPlaying, inReverse, showCopy, deckLength, assetPaths, aspectRatio }) => {

  const imagesPath = assetPaths.images + '/',
        videosPath = assetPaths.videos + '/';

  return (
    <div
      tabIndex="0"
      className="slide"
      style={{
        backgroundImage: 'url(' + imagesPath + (isPlaying && showCopy || inReverse ? slide.nextImage : slide.image) + ')',
      }}
    >

      <Copy
        handleNext={handleNext}
        activeSlide={activeSlide}
        slide={slide}
        nextSlideContent={nextSlideContent}
        isPlaying={isPlaying}
        inReverse={inReverse}
        showCopy={showCopy}
        isEndcap={(slide.isEndcap || ((showCopy && isPlaying || inReverse) && (deckLength - 2) == activeSlide) ? ' end-cap' : '')}
      />

      <VideoPlayer
        videoSrc={!slide.isEndcap ? videosPath + slide.videoSrc : ''}
        image={imagesPath + slide.image}
        nextImage={imagesPath + slide.nextImage}
        handleOnEnded={handleOnEnded}
        handleOnLoaded={handleOnLoaded}
        handleShowCopy={handleShowCopy}
        isPlaying={isPlaying}
        inReverse={inReverse}
        showCopyTiming={nextSlideContent.showCopyTiming}
        aspectRatio={aspectRatio}
      />

      <img
        className="imgCache"
        src={imagesPath + (isPlaying && !inReverse ? slide.nextImage : slide.image)}
        style={{left: 0, top: 0, visibility: 'hidden', position: 'absolute', zIndex: '-1'}} />

    </div>
  );

};

export default Slide
