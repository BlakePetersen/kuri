import React from 'react'
import Buttons from '../containers/Buttons'
import Slides from '../containers/Slides'
import Interactions from '../containers/Interactions'
const Swipeable = require('react-swipeable');
require("../scss/stylesheet.scss");

const App = ({assetPaths, aspectRatio, handleNext, handlePrev, isPlaying, canReverse}) => {

  const _handleNext = () => {
    return handleNext(isPlaying);
  };

  const _handlePrev = () => {
    return handlePrev(isPlaying, canReverse);
  };

  return (
    <div
      className="kuri-fnf"
    >
      <Swipeable
        onSwipedUp={_handleNext}
        onSwipedDown={_handlePrev}
        onSwipedLeft={_handleNext}
        onSwipedRight={_handlePrev}
      >
        <Interactions
          isPlaying={isPlaying}
          canReverse={isPlaying}
        />
      </Swipeable>
      <Slides
        assetPaths={assetPaths}
        aspectRatio={aspectRatio}
      />
      <Buttons />
    </div>
  )};

export default App
