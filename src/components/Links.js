import React from 'react'
import Link from './Link'

const Links = ({activeSlide, deckLength, isPlaying, inReverse, jumpToSlide, handleArrow}) => {
  let links = [],
    slide = inReverse ? activeSlide + 1 : activeSlide;

  for (let i = 0; i < deckLength; i++) {
    links.push(<Link
      activeSlide={slide}
      inReverse={inReverse}
      jumpToSlide={jumpToSlide}
      id={i}
      key={i.toString()}
    />)
  }
  return (
    <div
      tabIndex="-1"
      className={'slide-links'}
    >
      <button
        className={(((slide == 0 && !isPlaying) || isPlaying || inReverse) ? 'hide' : '')}
        onClick={e => {
         e.preventDefault();
         e.target.blur();
         handleArrow('prev', isPlaying)
       }}
      >
        <svg height='16' width='16' viewBox='0 0 64 64'>
          <polygon fill='none' stroke='#000000' strokeWidth='6' points='30,10 55,55 5,55'/>
        </svg>
      </button>
      {links}
      <button
        className={(((slide == deckLength - 1  && !inReverse) || isPlaying || inReverse ) ? 'hide' : '')}
        onClick={e => {
         e.preventDefault();
          e.target.blur();
          handleArrow('next', isPlaying);
       }}
      >
        <svg height='16' width='16' viewBox='0 0 64 64'>
          <polygon fill='none' stroke='#000000' strokeWidth='6' points='30,50 55,5 5,5'/>
        </svg>
      </button>
    </div>
    )
}

export default Links
