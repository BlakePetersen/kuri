import React from 'react'

const Link = ({ activeSlide, id, jumpToSlide, inReverse }) => {

  return (
    <a
      href="#"
       onClick={e => {
         e.preventDefault();
         e.target.blur();
         jumpToSlide(id);
       }}
       className={
        activeSlide == id ? 'active' : ''
       }
    >
    </a>
  )
};

export default Link
